$(document).ready(function () {
    carregaApelido();
    carregaContatos();
    carregaChat(true);
    $("#texto").scrollTop($("#texto")[0].scrollHeight);
});

var funcaoCarrega;

function carregaChat(primeiraCarga) {
    if ($("#destinatario").val() != 0) {
        if (!primeiraCarga) {
            primeiraCarga = false;
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: "ChatServlet",
            data: $("#ultimaMensagem,#destinatario").serialize() + "&mode=verificaNovasMensagens&primeiraCarga=" + primeiraCarga,
            success: function (data) {
                if (data && data[0].length > 0) {
                    for (var i = 0; i < data[0].length; i++) {
                        $("#texto").append("<p>" + data[0][i].mensagem + "</p>");
                    }
                    $("#ultimaMensagem").val(data[0][data[0].length - 1].id);
                    $("#texto").scrollTop($("#texto")[0].scrollHeight);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                location.reload();
            },
            complete: function (jqXHR, textStatus) {
                clearTimeout(funcaoCarrega);
                funcaoCarrega = setTimeout(carregaChat, 3000);
            }
        });
    } else {
        clearTimeout(funcaoCarrega);
        funcaoCarrega = setTimeout(carregaChat, 3000);
    }
}

$("#enviar").on("click", function () {
    enviarMensagem();
});

$("#mensagem").on("keydown", function (e) {
    if (e.keyCode == 13) {
        enviarMensagem();
    }
});

function enviarMensagem() {
    if ($("#destinatario").val() != 0) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "ChatServlet",
            data: $("#mensagem,#destinatario").serialize() + "&mode=inserirMensagem",
            success: function (data) {
                $("#mensagem").val("").focus();
            },
            error: function (jqXHR, textStatus, errorThrown) {
            },
            complete: function (jqXHR, textStatus) {
                clearTimeout(funcaoCarrega);
                carregaChat();
            }
        });
    }
}

function carregaApelido() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "ChatServlet",
        data: {mode: "carregaApelido"},
        success: function (data) {
            if (data) {
                $("#apelido").html(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            location.reload();
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

function carregaContatos() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "ChatServlet",
        data: {mode: "carregaContatos"},
        success: function (data) {
            if (data && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $("#destinatario").append('<option value="' + data[i].id + '">' + data[i].apelido + "</option>");
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            location.reload();
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

$("#destinatario").on("change", function () {
    $("#ultimaMensagem").val("0");
    $("#texto").html("");
    clearTimeout(funcaoCarrega);
    carregaChat(true);
});