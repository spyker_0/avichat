/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.MensagemDTO;
import DTO.UsuarioDTO;
import static Util.Conexao.getConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author spyker
 */
public class UsuarioDAO {

    public String retornarNomePorId(long idUsuario) {
        String retorno = "";
        try {
            Connection con = getConexao();
            String sql = "SELECT apelido FROM usuario WHERE id = " + idUsuario;
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("apelido");
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public long validarUsuario(String apelido, String senha) {
        long id = (long) 0;
        try {
            Connection con = getConexao();
            String sql = "SELECT id FROM usuario WHERE apelido = '" + apelido + "' AND senha = MD5('" + senha + "')";

            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getLong("id");
            }
            rs.close();
            ps.close();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
            return id;
        }
        return id;
    }

    public boolean verificaNomeExiste(String apelido) {
        boolean existe = false;
        try {
            Connection con = getConexao();
            String sql = "SELECT id FROM usuario WHERE apelido = '" + apelido + "';";

            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            existe = rs.next();
            rs.close();
            ps.close();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
            return existe;
        }
        return existe;
    }

    public void insere(UsuarioDTO usuario) {
        try {
            Connection con = getConexao();
            String sql = "INSERT INTO usuario(apelido,senha) VALUES ('" + usuario.getApelido() + "', MD5('" + usuario.getSenha() + "'));";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<UsuarioDTO> retornaUsuarios(long usuario) {
        ArrayList<UsuarioDTO> retorno = new ArrayList<UsuarioDTO>();
        try {
            Connection con = getConexao();
            String sql = "SELECT id,apelido FROM usuario WHERE id <> " + usuario;

            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                retorno.add(new UsuarioDTO(
                        rs.getInt("id"),
                        rs.getString("apelido")
                ));
            }

            rs.close();
            ps.close();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<UsuarioDTO>();
        }
        return retorno;
    }
}
