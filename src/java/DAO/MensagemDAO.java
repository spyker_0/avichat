/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.MensagemDTO;
import static Util.Conexao.getConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author spyker
 */
public class MensagemDAO {

    public void inserirMensagem(MensagemDTO mensagem) {
        try {
            Connection con = getConexao();
            String sql = "INSERT INTO mensagem(usuarioId, destinatarioId, mensagem) VALUES(?, ?, ?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, mensagem.getUsuarioId());
            ps.setLong(2, mensagem.getDestinatarioId());
            ps.setString(3, mensagem.getMensagem());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<MensagemDTO> retornaNovasMensagens(long idUsuario, long idMensagem, boolean primeiraCarga, long usuarioOrigem) {
        ArrayList<MensagemDTO> mensagens = new ArrayList();
        try {
            Connection con = getConexao();
            String sql = "";
            if (primeiraCarga) {
                sql = "SELECT * FROM("
                        + " SELECT id, mensagem, usuarioId "
                        + " FROM mensagem "
                        + " WHERE ((destinatarioId = " + idUsuario + " AND usuarioId = " + usuarioOrigem + ")"
                        + " OR (destinatarioId = " + usuarioOrigem + " AND usuarioId = " + idUsuario + "))"
                        + " ORDER BY id DESC LIMIT 50) as foo"
                        + " ORDER BY id ASC;";
            } else {
                sql = "SELECT id, mensagem, usuarioId FROM mensagem "
                        + " WHERE ((destinatarioId = " + idUsuario + " AND usuarioId = " + usuarioOrigem + ")"
                        + " OR (destinatarioId = " + usuarioOrigem + " AND usuarioId = " + idUsuario + "))"
                        + " AND id > " + idMensagem + " ORDER BY id;";
            }
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            UsuarioDAO uDAO = new UsuarioDAO();

            while (rs.next()) {
                mensagens.add(new MensagemDTO(
                        rs.getLong("id"),
                        uDAO.retornarNomePorId(rs.getLong("usuarioId")) + ": " + rs.getString("mensagem")
                ));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MensagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }
}
