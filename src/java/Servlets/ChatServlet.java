/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAO.MensagemDAO;
import DAO.UsuarioDAO;
import DTO.MensagemDTO;
import DTO.UsuarioDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.*;

/**
 *
 * @author spyker
 */
@WebServlet(name = "ChatServlet", urlPatterns = {"/ChatServlet"})
public class ChatServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            long usuarioLogado = (long) request.getSession().getAttribute("usuario");

            MensagemDAO mDAO = new MensagemDAO();

            String modo = request.getParameter("mode");
            if ("inserirMensagem".equals(modo)) {
                String mensagemParametro = request.getParameter("mensagem");
                if (mensagemParametro != null && !mensagemParametro.trim().equals("")) {
                    long usuarioDestino = Long.parseLong(request.getParameter("destinatario"));
                    MensagemDTO mensagem = new MensagemDTO(usuarioLogado, usuarioDestino, mensagemParametro);
                    mDAO.inserirMensagem(mensagem);
                }
                String json = new Gson().toJson("");
                out.print(json);
            } else if (modo.equals("verificaNovasMensagens")) {
                try {
                    Long ultimaMensagem = Long.parseLong(request.getParameter("ultimaMensagem"));
                    boolean primeiraCarga = Boolean.parseBoolean(request.getParameter("primeiraCarga"));
                    long usuarioOrigem = Long.parseLong(request.getParameter("destinatario"));
                    ArrayList<MensagemDTO> retorno = mDAO.retornaNovasMensagens(usuarioLogado, ultimaMensagem, primeiraCarga, usuarioOrigem);
                    if (retorno.size() > 0) {
                        ArrayList<Object> lista = new ArrayList<Object>();
                        lista.add(retorno);
                        String json = new Gson().toJson(lista);
                        out.print(json);
                    } else {
                        String json = new Gson().toJson("");
                        out.print(json);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                    String json = new Gson().toJson("");
                    out.print(json);
                }
            } else if (modo.equals("carregaApelido")) {
                UsuarioDAO uDAO = new UsuarioDAO();
                String retorno = uDAO.retornarNomePorId(usuarioLogado);
                String json = new Gson().toJson(retorno);
                out.print(json);
            } else if (modo.equals("carregaContatos")) {
                UsuarioDAO uDAO = new UsuarioDAO();
                String json = new Gson().toJson(uDAO.retornaUsuarios(usuarioLogado));
                out.print(json);
            } else {
                String json = new Gson().toJson("");
                out.print(json);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
