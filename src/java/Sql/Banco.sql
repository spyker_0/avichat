CREATE DATABASE avichat;

CREATE TABLE usuario(
    id SERIAL,
    apelido VARCHAR(20) UNIQUE,
    senha VARCHAR(32)
);

CREATE TABLE mensagem(
    id SERIAL,
    usuarioId bigint(20) REFERENCES usuario(id),
    mensagem VARCHAR(140),
    destinatarioId bigint(20) REFERENCES usuario(id)
);