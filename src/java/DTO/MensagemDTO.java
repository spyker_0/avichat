/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author spyker
 */
public class MensagemDTO {
    private long id;
    private long usuarioId;
    private long destinatarioId;
    private String mensagem;
    
    public MensagemDTO(long usuarioId, long destinatarioId, String mensagem) {
        this.usuarioId = usuarioId;
        this.destinatarioId = destinatarioId;
        this.mensagem = mensagem;
    }

    public MensagemDTO(long id, String mensagem){
        this.id = id;
        this.mensagem = mensagem;
    }
    
    public long getId() {
        return id;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public long getDestinatarioId() {
        return destinatarioId;
    }

    public String getMensagem() {
        return mensagem;
    }

}
