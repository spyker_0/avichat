/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author spyker
 */
public class UsuarioDTO {
    private int id;
    private String apelido;
    private String senha;

    public UsuarioDTO(int id, String apelido, String senha) {
        this.id = id;
        this.apelido = apelido;
        this.senha = senha;
    }

    public UsuarioDTO(String apelido, String senha) {
        this.apelido = apelido;
        this.senha = senha;
    }

    public UsuarioDTO(int id, String apelido) {
        this.id = id;
        this.apelido = apelido;
    }

    public int getId() {
        return id;
    }

    public String getApelido() {
        return apelido;
    }

    public String getSenha() {
        return senha;
    }
    
    
}
